package main

import (
	"bitbucket.org/cygnini/cygses"
	"fmt"
	"log"
	"net/http"
)

func setHandle(w http.ResponseWriter, r *http.Request) {
	ses := cygses.GetSession(w, r)
	ses.Set("user", "admin")
	log.Println("session set.")
}

func getHandle(w http.ResponseWriter, r *http.Request) {
	ses := cygses.GetSession(w, r)
	_, sesVal := ses.Get("user")
	fmt.Fprint(w, sesVal)
	log.Println("session got.")
}

func killHandle(w http.ResponseWriter, r *http.Request) {
	ses := cygses.GetSession(w, r)
	ses.Kill()
	log.Println("session killed")
}

func expandHandle(w http.ResponseWriter, r *http.Request) {
	ses := cygses.GetSession(w, r)
	ses.ExpandShortSession()
	log.Println("short session expanded")

	//This will fail
	cygses.RunOverseer()
}

/*func sesexpandHandle(w http.ResponseWriter, r *http.Request) {
	_, ses := cygses.GetSession(w, r)
	ses.Expand(time.Hour, w) //w is required for http.SetCookie()
	log.Println("session expanded")
}*/

func main() {
	cygses.Settings.AutoOverseer = false

	http.HandleFunc("/set/", setHandle)
	http.HandleFunc("/get/", getHandle)
	http.HandleFunc("/kill/", killHandle)
	http.HandleFunc("/expand/", expandHandle)
	//http.HandleFunc("/sesexpand/", sesexpandHandle)

	e := http.ListenAndServe(":1337", nil)
	if e != nil {
		log.Fatal(e)
	}
}
